var Song = Backbone.Model.extend({
    defaults: {
        genre: "Jazz"
    },
    validate: function(attrs) {
        if (!attrs.title) {
            return "Title is required"
        }
        if ( attrs.year < 2000 ) {
            return 'wrong year ';
         }
    },
    initialize: function(){
        console.log('a new song has been created');
    }
})

var song = new Song({
    title: "Mike",
    artist: "Mile",
    year: 1999
});

song.isValid()


// model inheritance
var Animal = Backbone.Model.extend({
    walk: function() {
        console.log("walk");
    }
});
var Dog = Animal.extend({
    walk: function(){
        Animal.prototype.walk.apply(this);
        console.log("dog walk");
    }
});
var dog = new Dog();
dog.walk();